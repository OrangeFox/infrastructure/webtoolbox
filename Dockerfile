#Stage 1
FROM docker.io/node:lts-alpine as builder
WORKDIR /app
COPY package*.json .
COPY yarn*.lock .
RUN yarn install --network-timeout 1000000
COPY . .
RUN yarn build

#Stage 2
FROM docker.io/nginx:stable-alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
ENTRYPOINT ["nginx", "-g", "daemon off;"]