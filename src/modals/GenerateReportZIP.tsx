import {SummarizeTwoTone} from "@mui/icons-material";
import {reportZip} from "../utils/reportZip";
import {Alert, Button, LinearProgress, Modal, ModalClose, Sheet, Typography} from "@mui/joy";
import React, {useEffect, useState} from "react";
import {ReleaseType} from "../types/release";


export function GenerateReportZIP({release}: { release: ReleaseType }) {
  const [open, setOpen] = useState(false);
  const [state, setState] = useState<'work' | 'progress' | 'done'>('work')

  useEffect(() => {
    if (!open) {
      return
    }

    setState('progress');
    reportZip(release).catch(console.error).finally(() => setState('done'));
  }, [open, release]);

  return <>
    <Button
      fullWidth
      variant="outlined"
      color="neutral"
      startDecorator={<SummarizeTwoTone/>}
      onClick={() => setOpen(true)}
    >Generate report ZIP
    </Button>

    <Modal
      aria-labelledby="modal-title"
      aria-describedby="modal-desc"
      open={open}
      onClose={() => setOpen(false)}
      sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
    >
      <Sheet
        variant="outlined"
        sx={{
          width: 300,
          maxWidth: 500,
          borderRadius: 'md',
          p: 3,
          boxShadow: 'lg',
        }}
      >
        <ModalClose variant="plain" sx={{m: 1}}/>
        <Typography
          component="h2"
          id="modal-title"
          level="h4"
          textColor="inherit"
          fontWeight="lg"
          mb={1}>Generate report ZIP
        </Typography>

        {state === 'progress' && <>
            <LinearProgress/>
            <Alert color="warning">
                Pulling the data from your device. Please be patient!
            </Alert>
        </>}

        {state === 'done' && <>
            <Alert color="success">
                The report ZIP has been generated.
                Please use it to report issues / bugs within OrangeFox Recovery on your device.
            </Alert>
        </>}
      </Sheet>
    </Modal>

  </>
}