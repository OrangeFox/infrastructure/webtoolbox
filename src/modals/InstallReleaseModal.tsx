import {ReleaseShortType} from "../types/release";
import {
  Alert,
  Button,
  Card,
  CircularProgress,
  LinearProgress,
  Modal,
  ModalClose,
  Sheet,
  Stack,
  Typography
} from "@mui/joy";
import {InstallMobileTwoTone} from "@mui/icons-material";
import React, {useState} from "react";
import {useGetRelease} from "../queries/getRelease";
import {installNewRelease} from "../utils/installNewRelease";


function ReleaseInstallInfo({releaseID}: { releaseID: string }) {

  const [isInstalling, setIsInstalling] = useState(false);

  const release = useGetRelease(undefined, releaseID);

  if (release.isFetching) {
    return <CircularProgress/>
  }

  if (!release.data) {
    return <></>
  }

  const rdata = release.data;

  return <Stack spacing={2}>
    <Card>
      <Typography level="title-lg">Changelog</Typography>
      <Typography level="body-sm" style={{whiteSpace: "pre-wrap"}}>
        {rdata.changelog.join('\n')}
      </Typography>
    </Card>

    {rdata.notes && <Card>
        <Typography level="title-lg">Notes</Typography>
        <Typography level="body-sm" style={{whiteSpace: "pre-wrap"}}>
          {rdata.notes}
        </Typography>
    </Card>}

    {rdata.bugs && <Card color="warning">
        <Typography level="title-lg">Bugs</Typography>
        <Typography level="body-sm" style={{whiteSpace: "pre-wrap"}}>
          {rdata.bugs.join('\n')}
        </Typography>
    </Card>}

    <Button
      fullWidth
      variant="solid"
      startDecorator={<InstallMobileTwoTone/>}
      onClick={() => {
        setIsInstalling(true);
        installNewRelease(rdata).catch(console.error).then(() => window.location.reload())
      }}
      disabled={isInstalling}
    >Install</Button>
    {isInstalling && <>
        <Alert color="warning">
            Please wait and do not touch the device!
        </Alert>
        <LinearProgress/>
    </>}

  </Stack>

}


export function InstallReleaseModal({releaseUpdate}: { releaseUpdate: ReleaseShortType }) {
  const [open, setOpen] = useState(false);

  return <>
    <Button
      startDecorator={<InstallMobileTwoTone/>}
      variant="soft"
      color="neutral"
      onClick={() => setOpen(true)}
    >Install...
    </Button>

    <Modal
      aria-labelledby="modal-title"
      aria-describedby="modal-desc"
      open={open}
      onClose={() => setOpen(false)}
      sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
    >
      <Sheet
        variant="outlined"
        sx={{
          width: 500,
          maxWidth: 500,
          borderRadius: 'md',
          p: 3,
          boxShadow: 'lg',
        }}
      >
        <ModalClose variant="plain" sx={{m: 1}}/>
        <Typography
          component="h2"
          id="modal-title"
          level="h4"
          textColor="inherit"
          fontWeight="lg"
          mb={1}>Install new Release</Typography>
        <ReleaseInstallInfo releaseID={releaseUpdate.id}/>
      </Sheet>
    </Modal>
  </>
}