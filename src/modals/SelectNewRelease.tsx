import {InstallMobileOutlined} from "@mui/icons-material";
import {Alert, Button, Card, Chip, LinearProgress, Modal, ModalClose, Sheet, Stack, Typography} from "@mui/joy";
import React, {useState} from "react";
import {useGetReleases} from "../queries/getReleases";
import {InstallReleaseModal} from "./InstallReleaseModal";
import {ReleaseShortType} from "../types/release";


function ReleaseCard({release}: { release: ReleaseShortType }) {
  const releaseDate = new Date(release.date * 1000)


  return <Card>
    <Typography level="body-sm">
      Version: <Chip>{release.version}</Chip><br/>
      Type: <Chip>{release.type}</Chip><br/>
      Release date: <Chip>{releaseDate.toDateString()}</Chip>
    </Typography>

    <InstallReleaseModal releaseUpdate={release}/>
  </Card>
}


export function SelectNewRelease({deviceID}: { deviceID: string }) {
  const [open, setOpen] = useState(false);

  const supportedReleases = useGetReleases(deviceID);

  if (supportedReleases.isFetching) {
    return <LinearProgress/>
  }

  if (supportedReleases.error || !supportedReleases.data) {
    return <Alert color="danger">
      Could not find the releases.
    </Alert>
  }

  return <>
    <Button
      variant="plain"
      color="neutral"
      startDecorator={<InstallMobileOutlined/>}
      onClick={() => setOpen(true)}
    >Install other release
    </Button>

    <Modal
      aria-labelledby="modal-title"
      aria-describedby="modal-desc"
      open={open}
      onClose={() => setOpen(false)}
      sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
    >
      <Sheet
        variant="outlined"
        sx={{
          width: 500,
          maxWidth: 500,
          borderRadius: 'md',
          p: 3,
          boxShadow: 'lg',
        }}
      >
        <ModalClose variant="plain" sx={{m: 1}}/>
        <Typography
          component="h2"
          id="modal-title"
          level="h4"
          textColor="inherit"
          fontWeight="lg"
          mb={1}>Install new Release</Typography>

        <Stack spacing={1}>
          {supportedReleases.data.data.map((release) =>
            <ReleaseCard key={release.id} release={release}/>
          )}
        </Stack>
      </Sheet>
    </Modal>

  </>
}