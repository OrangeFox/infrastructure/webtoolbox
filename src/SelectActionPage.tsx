import {InfoTwoTone} from "@mui/icons-material";
import {Alert, Button, Card, Chip, Grid, LinearProgress, Stack, Typography} from "@mui/joy";
import React, {useEffect, useState} from "react";
import {getProp} from "./utils/adb";
import {adbSingleton} from "./singletons/adb";
import {useGetRelease} from "./queries/getRelease";
import {ReleaseShortType, ReleaseType} from "./types/release";
import {useGetReleaseUpdate} from "./queries/getReleaseUpdate";
import {useGetDevice} from "./queries/getDevice";
import {InstallReleaseModal} from "./modals/InstallReleaseModal";
import {GenerateReportZIP} from "./modals/GenerateReportZIP";
import {SelectNewRelease} from "./modals/SelectNewRelease";


function UpdateCard({releaseUpdate}: { releaseUpdate: ReleaseShortType }) {
  const releaseType = releaseUpdate.type.charAt(0).toUpperCase() + releaseUpdate.type.slice(1);

  console.log(releaseUpdate.date)
  const releaseDate = new Date(releaseUpdate.date * 1000)

  return <Card color="primary">
    <Typography level="title-lg">New {releaseType} release</Typography>
    <Typography level="body-sm">
      Version: <Chip>{releaseUpdate.version}</Chip><br/>
      Release date: <Chip>{releaseDate.toDateString()}</Chip>
    </Typography>

    <InstallReleaseModal releaseUpdate={releaseUpdate}/>
  </Card>
}


function ReleaseUpdate({release}: { release: ReleaseType }) {
  const stableUpdate = useGetReleaseUpdate(release, 'stable');
  const betaUpdate = useGetReleaseUpdate(release, 'beta');

  return <>
    <br/>
    <Grid container direction="row" justifyContent="space-between">
      <Typography level="h4">Updates</Typography>
      <SelectNewRelease deviceID={release.device_id}/>
    </Grid>
    {(stableUpdate.data || betaUpdate.data) ? <Grid container direction="row" gap={1} sx={{flexGrow: 1}}>
        {stableUpdate.data && <Grid xs><UpdateCard releaseUpdate={stableUpdate.data}/></Grid>}
        {betaUpdate.data && <Grid xs><UpdateCard releaseUpdate={betaUpdate.data}/></Grid>}
      </Grid>
      : <Alert color="success">No updates available</Alert>}
  </>
}


function DeviceTitle({release}: { release: ReleaseType }) {
  const device = useGetDevice(release.device_id);

  if (!device.data) {
    return <></>
  }

  return <Typography level="title-lg">{device.data.full_name}</Typography>
}


function DeviceNotes({release}: { release: ReleaseType }) {
  const device = useGetDevice(release.device_id);

  if (!device.data) {
    return <></>
  }

  return <>
    {device.data.notes && <Alert variant="soft">
        <Typography level="body-sm" style={{whiteSpace: "pre-wrap"}}>
          {device.data.notes}
        </Typography>
    </Alert>}
  </>
}


function ReleaseInfo({buildID}: { buildID: string }) {
  const release = useGetRelease(buildID);

  if (release.isFetching) {
    return <LinearProgress/>
  }

  if (release.error || !release.data) {
    return <Alert color="danger">
      Could not find the release by its ID.
    </Alert>
  }

  const rdata: ReleaseType = release.data;

  return <Stack spacing={1}>
    <Card variant="outlined">
      <DeviceTitle release={rdata}/>
      <Typography level="body-sm">
        Version: <Chip>{rdata.version}</Chip><br/>
        Type: <Chip>{rdata.type}</Chip>
      </Typography>

      {rdata.notes && <Card color="success">
          <Typography level="title-lg" color="primary">Release notes</Typography>
          <Typography level="body-sm">
            {rdata.notes}
          </Typography>
      </Card>}

      <Button
        size="sm"
        color="neutral"
        variant="soft"
        startDecorator={<InfoTwoTone/>}
        onClick={() => window.open(rdata.url)}
      >Release info</Button>
    </Card>

    <DeviceNotes release={rdata}/>

    <ReleaseUpdate release={rdata}/>

    <Typography level="h4">Actions</Typography>
    <GenerateReportZIP release={rdata}/>

    {/*<Button*/}
    {/*  fullWidth*/}
    {/*  variant="outlined"*/}
    {/*  color="neutral"*/}
    {/*  startDecorator={<ArchiveTwoTone/>}*/}
    {/*>Sideload ZIP*/}
    {/*</Button>*/}
    {/*<Button fullWidth variant="outlined" startDecorator={<ArchiveTwoTone/>} disabled>Sideload ZIP</Button>*/}
  </Stack>
}


export function SelectActionPage() {
  const [buildID, setBuildID] = useState<string>('');
  const adb = adbSingleton.getInstance();

  useEffect(() => {
    getProp(adb, 'ro.build.fox_id').then(setBuildID);
  }, [adb]);

  const hasBuildID = buildID !== '';

  return <>
    {hasBuildID
      ? <ReleaseInfo buildID={buildID}/>
      : <Alert color="danger">
        Could not find the build ID. Please ensure that your device running OrangeFox Recovery.
      </Alert>
    }
  </>
}