import React from "react";
import {SelectDevicePage} from "./SelectDevicePage";
import {Box, Chip, CssVarsProvider, Grid, Typography} from "@mui/joy";
import {foxTheme} from "./utils/theme";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {AutoFixHighTwoTone} from "@mui/icons-material";


function App() {
  const queryClient = new QueryClient()

  return (
    <QueryClientProvider client={queryClient}>
      <CssVarsProvider disableTransitionOnChange theme={foxTheme} defaultMode="light">
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          py: 2,
          pb: 5,
        }}>
          <Grid container spacing={2} alignItems="center" alignSelf="center" justifyContent="center" direction="row">
            <Grid xs={12} sm={6} lg={5} display="flex" justifyContent="center">
              {/* Image, should be on left, and vertically centered */}
              <img src={require('./connect.png')} alt="Connection" style={{width: 'auto', height: '700px'}}/>
            </Grid>
            <Grid xs={12} sm={6} lg={5}>
              <Typography
                color="primary"
                level="h1"
                startDecorator={<AutoFixHighTwoTone/>}
                endDecorator={<Chip size="lg">Beta</Chip>}>
                Fox Toybox
              </Typography>
              <br/>
              {/* Main window, should be centered */}
              <SelectDevicePage/>
            </Grid>
          </Grid>
        </Box>
      </CssVarsProvider>
    </QueryClientProvider>
  );
}

export default App;
