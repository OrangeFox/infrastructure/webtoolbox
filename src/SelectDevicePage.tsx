import {Button, Typography} from "@mui/joy";
import React, {Dispatch, SetStateAction, useCallback, useState} from "react";
import {deviceSingleton} from "./singletons/device";
import {adbSingleton} from "./singletons/adb";
import {CredentialStore, getDevice} from "./utils/adb";
import {Adb, AdbDaemonTransport} from "@yume-chan/adb";
import {SelectActionPage} from "./SelectActionPage";


async function selectDevice(setIsConnected: Dispatch<SetStateAction<boolean>>) {
  try {
    const device: USBDevice = await navigator.usb.requestDevice({
      filters: [
        {
          classCode: 0xff,
          subclassCode: 0x42,
          protocolCode: 1,
        },
      ],
    });
    deviceSingleton.setInstance(device);

    const adbDevice = await getDevice();
    const connection = await adbDevice.connect();

    const transport = await AdbDaemonTransport.authenticate({
      serial: adbDevice.serial,
      connection,
      credentialStore: CredentialStore,
    })

    const adb: Adb = new Adb(transport);

    adbSingleton.setInstance(adb)
    setIsConnected(true);
  } catch (e) {
    if (e instanceof DOMException && e.name === "NotFoundError") {
      alert("No device selected or found, please check your connection and try again.");
      return;
    }

    throw e;
  }
}


export function SelectDevicePage() {
  const [isConnected, setIsConnected] = useState<boolean>(false);

  const onClick = useCallback(() => {
    selectDevice(setIsConnected).catch(console.error);
  }, []);

  if (isConnected) {
    return <SelectActionPage/>;
  }

  return <>
    <Typography>
      Please ensure that your USB cable is connected to your device while you're in OrangeFox Recovery mode.
    </Typography>

    <Button
      type="submit"
      fullWidth
      variant="solid"
      sx={{mt: 3, mb: 2}}
      onClick={onClick}
    >
      Find device
    </Button>
  </>;
}