export type DeviceShortType = {
  _id: string;
  codename: string;
  codenames: string[];
  full_name: string;
  id: string;
  model_name: string;
  model_names: string[];
  oem_name: string;
  supported: boolean;
  url: string;
};


export type MaintainerType = {
  _id: string;
  id: string;
  name: string;
  username: string;
};

export type DeviceType = DeviceShortType & {
  device_tree?: string;
  maintainer: MaintainerType;
  notes?: string;
};