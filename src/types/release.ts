export type Mirrors = {
  [key: string]: string;
};


export type ReleaseShortType = {
  _id: string;
  build_id: string;
  date: number;
  device_id: string;
  id: string;
  md5: string;
  size: number;
  type: string;
  version: string;
}

export type ReleaseType = ReleaseShortType & {
  filename: string;
  mirrors: Mirrors;
  notes?: string;
  bugs?: string[];
  changelog: string[];
  url: string;
};

export type ReleaseUpdateType = {
  data: ReleaseShortType[];
}