import {Singleton} from "../utils/singleton";
import {Adb} from "@yume-chan/adb";

export let adbSingleton = new Singleton<Adb>('adb');