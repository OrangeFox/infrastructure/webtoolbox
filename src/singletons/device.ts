import {Singleton} from "../utils/singleton";

export let deviceSingleton = new Singleton<USBDevice>('usbDevice');