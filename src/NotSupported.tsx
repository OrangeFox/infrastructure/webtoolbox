import {Alert} from "@mui/joy";

export function NotSupportedPage() {
  return <Alert color="danger" variant="soft">
    Unfortunately, your browser does not support Web USB protocol, please use a Chromium-based browser.
  </Alert>;
}