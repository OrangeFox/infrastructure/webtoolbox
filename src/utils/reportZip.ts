import {adbSingleton} from "../singletons/adb";
import {getConsoleOutput, getFilesInDir, pullFileAsBlob} from "./adb";
import JSZip from "jszip";
import {ReleaseType} from "../types/release";


const REPORT_DIR = '/sdcard/Fox/logs/'
const SCREENSHOTS_DIR = '/sdcard/Fox/screenshots/'


export async function reportZip(release: ReleaseType) {
  const adb = adbSingleton.getInstance();

  const zip = new JSZip();

  // Get latest logs
  const logsEntries = (await getFilesInDir(adb, REPORT_DIR))
    .filter((fn) => fn.endsWith('.log') || fn.endsWith('.log.zip'))
  await Promise.all(logsEntries.map(async (logEntry) => {
    const f = await pullFileAsBlob(adb, REPORT_DIR + logEntry);
    zip.file('logs/' + logEntry, f);
  }));

  // Get tmp log
  const f = await pullFileAsBlob(adb, '/tmp/recovery.log');
  zip.file('logs/recovery.log', f);

  // Get DMESG
  const dmesg = await getConsoleOutput(adb, 'dmesg');
  zip.file('logs/dmesg.log', dmesg);

  // Get logcat
  const logcat = await getConsoleOutput(adb, 'logcat -d');
  zip.file('logs/logcat.log', logcat);

  // Screenshots
  const ssEntries = (await getFilesInDir(adb, SCREENSHOTS_DIR))
    .filter((fn) => fn.endsWith('.png'))
  await Promise.all(ssEntries.map(async (entry) => {
    const f = await pullFileAsBlob(adb, SCREENSHOTS_DIR + entry);
    zip.file('screenshots/' + entry, f);
  }));

  // Include release object
  zip.file('release.json', JSON.stringify(release, null, '\t'));

  // Generate zip file and download it locally
  zip.generateAsync({type: "blob"}).then((content) => {
    const url = window.URL.createObjectURL(content);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'fox_report.zip';
    a.click();
    window.URL.revokeObjectURL(url);
  })

}
