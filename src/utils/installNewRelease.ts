import {ReleaseType} from "../types/release";
import {adbSingleton} from "../singletons/adb";
import {getConsoleOutput, pushFileAsBlob} from "./adb";


const TMP_FILE = '/tmp/newfox.zip'


export async function installNewRelease(release: ReleaseType) {
  const adb = adbSingleton.getInstance();

  const dlLink = Object.values(release.mirrors)[0];
  const releaseFile = await fetch(dlLink, {}).then((response) => response.blob());

  await getConsoleOutput(adb, 'rm ' + TMP_FILE);
  await pushFileAsBlob(adb, TMP_FILE, releaseFile);

  await getConsoleOutput(adb, 'twrp install ' + TMP_FILE);
  await getConsoleOutput(adb, 'twrp reboot recovery');
  adb.close();
}