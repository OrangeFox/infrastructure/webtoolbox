import {AdbDaemonWebUsbDevice, AdbDaemonWebUsbDeviceManager} from "@yume-chan/adb-daemon-webusb";
import AdbWebCredentialStore from "@yume-chan/adb-credential-web";
import {Adb} from "@yume-chan/adb";
import type {ReadableStream} from "@yume-chan/stream-extra";
import {DecodeUtf8Stream, WrapConsumableStream, WritableStream} from "@yume-chan/stream-extra";


const Manager: AdbDaemonWebUsbDeviceManager | undefined = AdbDaemonWebUsbDeviceManager.BROWSER;
export const CredentialStore: AdbWebCredentialStore = new AdbWebCredentialStore("OrangeFox Bug reporter");


export function isSupported(): boolean {
  return Manager !== undefined;
}


export async function getDevice(): Promise<AdbDaemonWebUsbDevice> {
  const devices = await Manager?.getDevices();

  if (!devices) {
    return Promise.reject(new Error("No devices found"));
  }

  return devices[0];
}


export async function getConsoleOutput(adb: Adb, cmd: string): Promise<string> {
  const process = await adb.subprocess.spawn(cmd);

  let output = '';

  await process.stdout.pipeThrough(new DecodeUtf8Stream()).pipeTo(
    new WritableStream<string>({
      write(chunk) {
        output += chunk;
      }
    }),
  );

  return output;
}


async function streamToBlob(stream: ReadableStream): Promise<Blob> {
  const chunks: Uint8Array[] = [];
  const reader = stream.getReader();

  try {
    while (true) {
      const {done, value} = await reader.read();
      if (done) break;
      chunks.push(value);
    }

    return new Blob(chunks, {type: 'application/octet-stream'});
  } finally {
    reader.releaseLock(); // Release the lock when finished
  }
}


export async function getProp(adb: Adb, propName: string): Promise<string> {
  let output = await getConsoleOutput(adb, `getprop ${propName}`);
  output = output.replace(/\n$/, '');
  return output
}


export async function pullFileAsBlob(adb: Adb, path: string): Promise<Blob> {
  const sync = await adb.sync();

  const content = sync.read(path);
  const blob = await streamToBlob(content);

  await sync.dispose();
  return blob;
}


export async function pushFileAsBlob(adb: Adb, path: string, file: Blob): Promise<undefined> {
  const sync = await adb.sync();

  await sync.write({
    filename: path,
    file: (file.stream() as never as ReadableStream<Uint8Array>).pipeThrough(new WrapConsumableStream()),
  });

  await sync.dispose();
  return undefined;
}


export async function getFilesInDir(adb: Adb, path: string): Promise<string[]> {
  const sync = await adb.sync();

  const entries = await sync.readdir(path);

  await sync.dispose();
  return entries.map(entry => entry.name);
}