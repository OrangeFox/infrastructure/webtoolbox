import {extendTheme} from "@mui/joy";

export const foxTheme = extendTheme({
  colorSchemes: {
    light: {
      palette: {
        // Primary brand color
        primary: {
          50: '#fff3e1',
          100: '#ffe0b0',
          200: '#ffcc7e',
          300: '#ffb74d',
          400: '#ffa726',
          500: '#ed7002', // Brand color
          600: '#e65c00',
          700: '#d84c00',
          800: '#c73e00',
          900: '#ae3200',
        },
      }
    },
    dark: {
      palette: {
        // Primary brand color
        primary: {
          50: '#fff3e1',
          100: '#ffe0b0',
          200: '#ffcc7e',
          300: '#ffb74d',
          400: '#ffa726',
          500: '#ed7002', // Brand color
          600: '#e65c00',
          700: '#d84c00',
          800: '#601e00',
          900: '#601e00',
        },
      }
    }
  },
});