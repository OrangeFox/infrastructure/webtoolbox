export class Singleton<T> {
  private value: T | undefined;
  private locked: boolean = false;

  constructor(private singletonName: string, private initializer?: () => T) {
  }

  setInstance(value: T): void {
    if (this.locked || this.value !== undefined) {
      throw new Error(`Setting a value for ${this.singletonName} that is already set globally as a singleton.`);
    }
    this.value = value;
  }

  getInstance(): T {
    if (!this.value) {
      if (!this.initializer) {
        const error = new Error(`Accessing global singleton '${this.singletonName}' that has not yet been set before or has been cleared again.`);
        console.error(error, error.stack);
        throw error;
      }
      this.value = this.initializer();
    }
    return this.value;
  }

  isSet(): boolean {
    console.log(this.value);
    return this.value !== undefined;
  }

  lock(): void {
    this.locked = true;
  }

  clear(): void {
    if (this.locked) {
      throw new Error(`Cannot clear value for ${this.singletonName} that is set as final singleton.`);
    }
    this.value = undefined;
  }

  getValueOrUndefined(): T | undefined {
    return this.value;
  }
}
