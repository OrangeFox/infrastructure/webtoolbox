import {useQuery} from '@tanstack/react-query';
import ky, {HTTPError} from 'ky';
import {DeviceType} from "../types/device";

export function useGetDevice(deviceID: string) {
  return useQuery<DeviceType | undefined>({
    queryKey: ['device', deviceID],
    queryFn: async () => {
      try {
        const searchParams = new URLSearchParams();
        searchParams.set('device_id', deviceID);

        const res = await ky.get('https://api.orangefox.download/devices/get', {searchParams});
        return res.json();
      } catch (err) {
        if (err instanceof HTTPError && err.response.status === 404) {
          return undefined;
        }
        throw err;
      }
    },
  });
}
