import {useQuery} from '@tanstack/react-query';
import ky from 'ky';
import {ReleaseUpdateType} from "../types/release";

export function useGetReleases(deviceID: string, type?: 'stable' | 'beta') {
  return useQuery<ReleaseUpdateType>({
    queryKey: ['releases', deviceID, type],
    queryFn: async () => {
      const searchParams = new URLSearchParams();

      searchParams.set('device_id', deviceID);

      if (type) {
        searchParams.set('type', type);
      }

      const res = await ky.get('https://api.orangefox.download/releases/', {searchParams});
      return res.json();
    },
  });
}
