import {useQuery} from '@tanstack/react-query';
import ky, {HTTPError} from 'ky';
import {ReleaseShortType, ReleaseUpdateType} from "../types/release";


export function useGetReleaseUpdate(release: ReleaseShortType, releaseType: 'stable' | 'beta') {
  return useQuery<ReleaseShortType | undefined>({
    queryKey: ['releaseUpdates', release.id, releaseType],
    queryFn: async () => {
      try {
        const searchParams = new URLSearchParams();
        searchParams.set('after_release_id', release.id);
        searchParams.set('device_id', release.device_id);
        searchParams.set('type', releaseType);
        searchParams.set('limit', '1');

        const res = await ky.get('https://api.orangefox.download/releases/', {searchParams});
        const releases: ReleaseUpdateType = await res.json();
        return releases.data[0];
      } catch (err) {
        if (err instanceof HTTPError && err.response.status === 404) {
          return undefined;
        }
        throw err;
      }
    },
  });
}
