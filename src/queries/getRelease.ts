import {useQuery} from '@tanstack/react-query';
import ky, {HTTPError} from 'ky';
import {ReleaseType} from "../types/release";

export function useGetRelease(buildID?: string, releaseID?: string) {
  return useQuery<ReleaseType | undefined>({
    queryKey: ['release', buildID, releaseID],
    queryFn: async () => {
      try {
        const searchParams = new URLSearchParams();

        if (buildID) {
          searchParams.set('build_id', buildID);
        }
        if (releaseID) {
          searchParams.set('release_id', releaseID);
        }

        const res = await ky.get('https://api.orangefox.download/releases/get', {searchParams});
        return res.json();
      } catch (err) {
        if (err instanceof HTTPError && err.response.status === 404) {
          return undefined;
        }
        throw err;
      }
    },
  });
}
